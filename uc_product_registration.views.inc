<?php
/**
 * @file
 * Enables Views 2.x+ compatibility for uc_product_registration.
 */

function uc_product_registration_views_data() {
  $data = array();

  $data['uc_registered_products']['table']['group'] = t('Product Registration');
  $data['uc_registered_products']['table']['join'] = array(
    'node' => array(
      'left_table' => 'node',
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'node_revision' => array(
      'left_table' => 'node_revision',
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );

  $data['uc_registered_products']['nid'] = array(
    'title' => t('Product ID'),
    'help' => t('The node ID of a product.'),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Product ID'),
    ),
  );

  $data['uc_registered_products']['vid'] = array(
    'title' => t('Product VID'),
    'help' => t('The node revision ID of a product.'),
    'argument' => array(
      'handler' => 'views_handler_argument_node_vid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'vid',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'vid',
      'handler' => 'views_handler_relationship',
      'label' => t('Product ID'),
    ),
  );

  $data['uc_registered_products']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The user who registered this product.'),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'relationship' => array(
      'base' => 'users',
      'field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User ID'),
    ),
  );

  $data['uc_registered_products']['registered'] = array(
    'title' => t('Registration Date'),
    'help' => t('Date the product was registered.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['uc_registered_products']['purchdate'] = array(
    'title' => t('Purchase Date'),
    'help' => t('Date the product was purchased.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['uc_registered_products']['model'] = array(
    'title' => t('Model Number'),
    'help' => t('The model number supplied by the user at registration.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['uc_registered_products']['serial'] = array(
    'title' => t('Serial Number'),
    'help' => t('The serial number supplied by the user at registration.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // extend the product table functionality
  $data['uc_registered_products']['is_registerable_product'] = array(
    'title' => t('Is Registerable'),
    'help' => t('Filters results based on whether or not which product classes are registerable.'),
    'group' => t('Product'),
    'filter' => array(
      'handler' => 'uc_product_registration_handler_filter_product_is_registerable',
      'label' => t('Is Registerable'),
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function uc_product_registration_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_product_registration') .'/views',
    ),
    'handlers' => array(
      'uc_product_registration_handler_filter_product_is_registerable' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
    ),
  );
}
