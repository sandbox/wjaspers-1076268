<?php
/**
 * @file
 * Adds per-product restrictions to product registration.
 */

/**
 * Monitors node activities and responds accordingly
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3
 *  Teaser String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 */
function uc_product_registration_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  // Prevent anonymous users from accessing any product registration information.
  if (uc_product_registration_checkanonymous()) {
    return;
  }
  // Prevent nodes which aren't permitted to have or manage product registration information from being manipulated here.
  if (NULL == $node) {
    return;
  }
  if (!uc_product_registration_checkproductclasses($node->type)) {
    return;
  }

  // Determine the operation to be performed
  switch ($op) {
    case 'insert':
      return uc_product_registration_nodeapi_insert($node, $op, $a3, $a4);
      break;
    case 'update':
      return uc_product_registration_nodeapi_update($node, $op, $a3, $a4);
      break;
    case 'load':
      return uc_product_registration_nodeapi_load($node, $op, $a3, $a4);
      break;
    case 'presave':
      return uc_product_registration_nodeapi_presave($node, $op, $a3, $a4);
      break;
    case 'validate':
      return uc_product_registration_nodeapi_validate($node, $op, $a3, $a4);
      break;
    case 'delete':
      return uc_product_registration_nodeapi_delete($node, $op, $a3, $a4);
      break;
    case 'delete revision':
      return uc_product_registration_nodeapi_delete_revision($node, $op, $a3, $a4);
      break;
    case 'view':
      // do nothing
      break;
  }
}

/**
 * Saves the product registration REGEX's appropriate to a NEW, specific product node.
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3 Teaser
 *  String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 */
function uc_product_registration_nodeapi_insert(&$node, $op, $a3=NULL, $a4=NULL) {
  uc_product_registration_on_nodesave($node, 'insert');
}

/**
 * Updates the product registration REGEX's appropriate to a specific product.
 * CRITICAL NOTE: This function is called AFTER the node is updated from
 * Drupal's core. The NEW vid will be made available by Drupal.
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3 Teaser
 *  String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 */
function uc_product_registration_nodeapi_update(&$node, $op, $a3=NULL, $a4=NULL) {
  uc_product_registration_on_nodesave($node, 'update');
}

/**
 * Internal function that determines what actions to take when a node
 * is updated or saved.
 * @param Object $node
 * @param String $action
 *  This will either be one of "update" or "insert" based on the nodeapi call this method is called from.
 */
function uc_product_registration_on_nodesave(&$node, $action='update') {
  // Defines which fields we'll save
  $fields = array(
    'nid' => $node->nid,
    'vid' => ($node->vid ? $node->vid : NULL),
    'model_expr' => $node->model_expr,
    'serial_expr' => $node->serial_expr,
  );
  // Ensure the model number expression isn't identical to the global setting.
  // if it is, set it to NULL.
  if ($node->model_expr == variable_get('uc_product_registration_global_model_expr', NULL)) {
    $fields['model_expr']  = NULL;
  }

  // Ensure the serial number expression isn't identical to the global setting.
  // If it is, set it to NULL.
  if ($node->serial_expr == variable_get('uc_product_registration_global_serial_expr', NULL)) {
    $fields['serial_expr'] = NULL;
  }
  // If the model/serial number expressions aren't defined, or were the same
  // as their global settings, don't save anything.
  if (empty($fields['model_expr']) && empty($fields['serial_expr'])) {
    return;
  }
  // Determine which action to take.
  switch ($action) {
    case 'insert':
      // Simple hook (new in D6.x?) to store the data above.
      drupal_write_record('uc_registered_products_formats', $fields);
      break;
    case 'update':
      // Look for a db entry
      $result = db_result(db_query("SELECT nid, vid FROM {uc_registered_products_formats} WHERE nid = %d AND vid = %d", $fields['nid'], $fields['vid']));
      // If one doesn't exist, treat this as an insert.
      if (empty($result['nid'])) {
        return uc_product_registration_on_nodesave($node, 'insert');
      }
      // Otherwise, treat this is an update
      db_query(
        "UPDATE {uc_registered_products_formats}
         SET model_expr = '%s',
             serial_expr = '%s'
         WHERE nid = %d
         AND   vid = %d",
        $fields['model_expr'],
        $fields['serial_expr'],
        $fields['nid'],
        $fields['vid']
      );
      //TODO: the update function doesn't seem to work correctly in D6.19
      // drupal_write_record('uc_registered_products_formats', $fields, $fields);
    break;
    // no default case.
  }
}

/**
 * Deletes a registered product's model/serial formats from the database
 * if its respective node is deleted.
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3 Teaser
 *  String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 */
function uc_product_registration_nodeapi_delete(&$node, $op, $a3=NULL, $a4=NULL) {
  // Delete products registered by users that match this NID
  // if the database enforces referential integrity, this may fail
  db_query('DELETE FROM {uc_registered_products} WHERE nid = %d', $node->nid);
  // Delete individual product settings
  db_query('DELETE FROM {uc_registered_products_formats} WHERE nid = %d', $node->nid);
}

/**
 * Deletes a registered product's model/serial formats from the database
 * if its respective node is deleted. This will NOT delete registered products
 * related to the revision in question.
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3 Teaser
 *  String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 */
function uc_product_registration_nodeapi_delete_revision(&$node, $op, $a3=NULL, $a4=NULL) {
  // Don't delete products that are still associated with this node
  // Just delete the format revision the admin/user didn't want
  db_query('DELETE FROM {uc_registered_products_formats} WHERE nid = %d AND vid = %d', $node->nid, $node->vid);
}

/**
 * Actions to perform just before saving the node.
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3 Teaser
 *  String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 */
function uc_product_registration_nodeapi_presave(&$node, $op, $a3=NULL, $a4=NULL) {
  // Validate the node before saving it.
  uc_product_registration_nodeapi_validate($node, $op, $a3, $a4);
}

/**
 * Validate the form input and respond accordingly.
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3 Teaser
 *  String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 */
function uc_product_registration_nodeapi_validate(&$node, $op, $a3=NULL, $a4=NULL) {
  // PHP problem: (5.2.14 <= ?)
  // PCRE module has ending delimeter issue with [/#!]$.
  // Circumvented by requiring ONE end delimeter.
  $valid_expr = "@^[\!\#/](.*)[\!\#/]{1}@ixu";

  // Get the form data needed to enforce product registration for this node.
  $model_expr = _uc_product_registration_format_expr($node->model_expr);
  $serial_expr = _uc_product_registration_format_expr($node->serial_expr);

  // Check to ensure the model number expression is valid
  if (!preg_match($valid_expr, $model_expr)) {
    form_set_error('uc_product_registration][model_expr', t('Your model number regular expression does not follow PCRE syntax.'));
  }
  // Check to ensure the serial number expression is valid
  if (!preg_match($valid_expr, $serial_expr)) {
    form_set_error('uc_product_registration][serial_expr', t('Your serial number regular expression does not follow PCRE syntax.'));
  }

}

/**
 * Retreives the product registration REGEX's appropriate to a specific product node.
 * @param Object $node
 *  A reference to the node Object being manipulated
 * @param String $op
 *  The operation to be performed.
 * @param mixed $a3 Teaser
 *  String or Form object in use.
 * @param mixed $a4
 *  Page (where used)
 * @return Array
 *  Returns an array of fields/values attached to the node by our module.
 */
function uc_product_registration_nodeapi_load(&$node, $op, $a3=NULL, $a4=NULL) {
  // Get the administrative variables that determine if global
  // formats should be enforced
  $use_global_model_expr  = variable_get('uc_product_registration_use_global_model_expr' , FALSE);
  $use_global_serial_expr = variable_get('uc_product_registration_use_global_serial_expr', FALSE);

  // Determine if a revision number to this node is being used.
  $vid = isset($node->vid) ? $node->vid : 1;

  // Lookup the model/serial number formats associated with this node.
  // NOTE: Even if the global setting exists, we ignore it here
  // The node edit form should ALWAYS display the format associated
  // directly with the node.
  $q1 = db_query("SELECT model_expr, serial_expr FROM {uc_registered_products_formats} WHERE nid = %d AND vid = %d", $node->nid, $node->vid);

  // If no data is found, this function will return FALSE.
  $data = db_fetch_array($q1);

  // If the model/serial number formats aren't specific to this node
  // provide defaults of "" or whatever the global expressions are
  // if and ONLY if the global formats are enforced.
  if (!$data) {
    $data = array(
      'model_expr' => ($use_global_model_expr ? variable_get('uc_product_registration_global_model_expr', NULL) : ""),
      'serial_expr' => ($use_global_serial_expr ? variable_get('uc_product_registration_global_serial_expr', NULL) : ""),
    );
  }

  // NOTE: uc_product_registration_load_product is a NODEAPI call, and is constructed onthefly
  $data = array_merge($data, module_invoke_all('uc_product_registration_load', $node));
  return array('uc_product_registration' => $data);
}

/**
 * Allows administrators to override model/serial number registrations on a per-node basis.
 * @param Array $form
 *  A keyed array of elements that define a form.
 * @param String $form_state
 * @param String $form_id
 *  The form_id identifies the form to alter.
 * @return Array
 */
function uc_product_registration_form_alter(&$form, &$form_state, $form_id) {
  $type = $form['#node_type']->type ? $form['#node_type']->type : $form['#node']->type;
  // Ensure the node_form is only altered if
  // this node belongs to a product class
  // that allows product registration
  if (!uc_product_registration_checkproductclasses($type)) {
    return;
  }
  // Attach form fields to the node form
  $form = array_merge($form, uc_product_registration_product_form_fields($form, $form_state));
}

/**
 * Obtains the fields necessary to store/manipulate acceptable
 * product model / serial numbers for registration.
 * @param Array $form
 *  A keyed array of elements that define a form.
 * @param String $form_state
 * @param String $form_id
 *  The form_id identifies the form to attach these fields to.
 * @return Array
 *  Returns an array of fields which will be added to the node-edit form.
 */
function uc_product_registration_product_form_fields(&$form, $form_state) {
  // Bring the node and its fields into context
  $node = $form['#node'];
  // Establish a placeholder for our new fields.
  $fields = array();
  // Create a fieldset to group our fields on the node form
  $fields['uc_product_registration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product Registration'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $model = $node->uc_product_registration['model_expr'];
  $fields['uc_product_registration']['model_expr'] = array(
    '#type' => 'textfield',
    '#title' => t('Model Number Expression'),
    '#description' => t('You may restrict products registered from this node by their model number with a regular expression. If the global setting is enforced, this field will be disabled. Do NOT include delimeters in your expression.'),
    '#default_value' => ($model ? $model : variable_get('uc_product_registration_global_model_expr', NULL)),
    '#maxlength' => 255,
    '#required' => FALSE,
    '#disabled' => variable_get('uc_product_registration_use_global_model_expr', FALSE),
  );
  $serial = $node->uc_product_registration['serial_expr'];
  $fields['uc_product_registration']['serial_expr'] = array(
    '#type' => 'textfield',
    '#title' => t('Serial Number Expression'),
    '#description' => t('You may restrict products registered from this node by their serial number with a regular expression. If the global setting is enforced, this field will be disabled. Do NOT include delimeters in your expression.'),
    '#default_value' => ($serial ? $serial : variable_get('uc_product_registration_global_serial_expr', NULL)),
    '#maxlength' => 255,
    '#required' => FALSE,
    '#disabled' => variable_get('uc_product_registration_use_global_serial_expr', FALSE),
  );
  // Enable the node to call our handler
  // when the node edit form is submitted
  if (is_array($fields['#submit']))
    array_unshift($fields['#submit'], '_uc_product_registration_product_form_handler');
  $fields['#submit'][] = '_uc_product_registration_product_form_handler';
  // Give the form handler our new fields
  return $fields;
}

/**
 * Handles any activities related to the form before saving the node.
 * @param Array $form
 * @param String $form_state
 * @return NULL
 *  Returns nothing.
 */
function uc_product_registration_product_form_handler(&$form, $form_state) {
  // As of now, we have nothing special to do to the form on submit
  return;
}
