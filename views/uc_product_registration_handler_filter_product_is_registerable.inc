<?php
/**
 * @file
 * Views handler: Node filter on whether or not the "product" is "registerable".
 */

/**
 * Filter nodes based on whether they have an entry in the uc_products table.
 */
class uc_product_registration_handler_filter_product_is_registerable extends views_handler_filter_boolean_operator {
  function query() {
    $types = variable_get('uc_product_registration_node_types', uc_product_types());
    foreach ($types as $t => $enabled) {
      if (0===$enabled) {
        unset($types[$t]);
      }
    }
    $this->query->add_field('node', 'type');
    $this->query->add_where($this->options['group'], "node.type ". (empty($this->value) ? "NOT " : "") ."IN (". db_placeholders($types, 'varchar') .")", $types);
  }
}
