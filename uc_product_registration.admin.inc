<?php
/**
 * @file
 * Administration page callbacks
 *
 * Creates an administration page for Product Registration settings.
 */

/**
 * Provides administrative settings for our module.
 * @ingroup forms
 * @see system_settings_form()
 */
function uc_product_registration_admin_settings() {
  // We must call this from Ubercart or we'll see ALL node types
  $options = uc_product_product_types();
  // Ubercart sends only a numerically indexed array
  $options = array_combine(array_values($options), array_values($options));
  // List the product classes as options for what the site admin(s) want to be "registerable products"
  $form['uc_product_registration_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Users may register products from these product classes'),
    '#options' => $options,
    '#default_value' => variable_get('uc_product_registration_node_types', $options),
    '#description' => t('Permits users to register products they may own that belong to the product classes defined in your store.'),
  );
  // Should the site require a purchase date at registration?
  $form['uc_product_registration_require_purchdate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require Purchase Date at registration?'),
    '#options' => array( 0 => 'No', 1 => 'Yes' ),
    '#default_value' => variable_get('uc_product_registration_require_purchdate', 0),
    '#description' => t('Require that the user supply a purchase date during product registration?'),
  );
  // The database supports both methods, but site admins can restrict whether or not a product can be registered to more than one user.
  $form['uc_product_registration_allow_multiple_registrations'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow more than one user to register a unique product?'),
    '#options' => array( 0 => 'No', 1 => 'Yes' ),
    '#default_value' => variable_get('uc_product_registration_allow_multiple_registrations', 0),
    '#description' => t('Should the system permit more than one user to register a unique product. A product is considered unique via its node id, model, and serial number.'),
  );
  // Allows admins to enforce a global regular expression against model numbers for registered products.
  $form['uc_product_registration_use_global_model_expr'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require a global model number regular expression?'),
    '#options' => array( 0 => 'No', 1 => 'Yes' ),
    '#default_value' => variable_get('uc_product_registration_use_global_model_expr', 1),
    '#description' => t('Should the system restrict product registrations by a model number validated by a regular expression that applies to ALL products?'),
  );
  // Allows admins to define a global regular expression that validates model numbers for registered products.
  $form['uc_product_registration_global_model_expr'] = array(
    '#type' => 'textfield',
    '#length' => 100,
    '#maxlength' => 255,
    '#title' => t('Global Model Number Regular Expression'),
    '#default_value' => variable_get('uc_product_registration_global_model_expr', NULL),
    '#description' => t('If you supply an expression, it must be PCRE compatible.'),
  );
  // Allows admins to enforce a global regular expression against serial numbers for registered products.
  $form['uc_product_registration_use_global_serial_expr'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require a global serial number regular expression?'),
    '#options' => array( 0 => 'No', 1 => 'Yes' ),
    '#default_value' => variable_get('uc_product_registration_use_global_serial_expr', 1),
    '#description' => t('Should the system restrict product registrations by a serial number validated by a regular expression that applies to ALL products?'),
  );
  // Allows admins to define a global regular expression that validates serial numbers for registered products.
  $form['uc_product_registration_global_serial_expr'] = array(
    '#type' => 'textfield',
    '#length' => 100,
    '#maxlength' => 255,
    '#title' => t('Global Serial Number Regular Expression'),
    '#default_value' => variable_get('uc_product_registration_global_serial_expr', NULL),
    '#description' => t('If you supply an expression, it must be PCRE compatible.'),
  );
  // Returns the "system settings" form.
  return system_settings_form($form);
}
