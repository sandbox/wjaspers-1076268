<?php
/**
 * @file
 * Rules (1.x for Drupal 6.x) integration for uc_product_registration.
 *
 * Adds specific actions that can be triggered and fire events
 * based on the rules module.
 */

/**
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 * @return Array
 *  Returns an array of events, keyed by callback function names,
 *  that can be triggered from this module.
 */
function uc_product_registration_rules_event_info() {
  $module = 'Ubercart Product Registration';
  $common_args = array(
    'customer' => array(
      'label' => t('Customer'),
      'type' => 'user',
    ),
    'product' => array(
      'label' => t('Product'),
      'type' => 'node',
    ),
    'registered_product' => array(
      'label' => t('Registered Product'),
      'type' => 'registered_product',
    ),
    'model' => array(
      'label' => t('Model Number'),
      'type' => 'String'
    ),
    'serial' => array(
      'label' => t('Serial Number'),
      'type' => 'String'
    ),
    'registered' => array(
      'label' => t('Date Registered'),
      'type' => 'Date'
    ),
    'purchdate' => array(
      'label' => t('Date of Purchase'),
      'type' => 'Date'
    ),
  );
  return array(
    'uc_product_registration_rules_product_registered_event' => array(
      'label' => t('A product is registered.'),
      'help' => t('Displays a message when a user finishes registering a product from your catalog.'),
      'module' => $module,
      'arguments' => $common_args,
    ),
    'uc_product_registration_rules_product_updated_event' => array(
      'label' => t('A registered product is updated.'),
      'help' => t('A user updates information regarding their registered product.'),
      'module' => $module,
      'arguments' => $common_args,
    ),
    'uc_product_registration_rules_product_deleted_event' => array(
      'label' => t('A registered product is deleted.'),
      'help' => t('A user deletes their registered product.'),
      'module' => $module,
      'arguments' => $common_args,
    ),
  );
}

/**
 * Defines conditions available to product registration rules.
 * @return Array
 *  Returns an array, keyed by callback function names that define
 *  conditions accessible to rules defined from this module.
 */
function uc_product_registration_rules_condition_info() {
  $module = 'Ubercart Product Registration';
  // Basic arguments related to product registration
  $common_args = array(
    'customer' => array(
      'label' => t('Customer'),
      'type' => 'user',
    ),
    'product' => array(
      'label' => t('Product'),
      'type' => 'node',
    ),
    'registered_product' => array(
      'label' => t('Registered Product'),
      'type' => 'registered_product',
    ),
    'model' => array(
      'label' => t('Model Number'),
      'type' => 'String',
    ),
    'serial' => array(
      'label' => t('Serial Number'),
      'type' => 'String',
    ),
    'registered' => array(
      'label' => t('Date Registered'),
      'type' => 'Date',
    ),
    'purchdate' => array(
      'label' => t('Date of Purchase'),
      'type' => 'Date',
    ),
    'text2' => array(
      'label' => t('Text 2'),
      'type' => 'String',
    ),
    'date2' => array(
      'label' => t('Date 2'),
      'type' => 'Date',
    ),
  );
  return array(
    'rules_condition_content_is_type' => array(
      'label' => t('Product is of class...'),
      'help' => t('TRUE if the product type is one of your selection of classes.'),
      'module' => $module,
      'arguments' => array(
        'node' => array(
          'type' => 'product',
          'label' => t('Product')
        ),
      ),
    ),
    'uc_product_registration_rules_condition_modelnumber_compare' => array(
      'label' => t('Compare a Model Number.'),
      'help' => t('TRUE if the model number is correct.'),
      'module' => $module,
      'arguments' => array( $common_args['model'], $common_args['text2']),
    ),
    'uc_product_registration_rules_condition_serialnumber_compare' => array(
      'label' => t('Compare a Serial Number.'),
      'help' => t('TRUE if the serial number is correct.'),
      'module' => $module,
      'arguments' => array($common_args['serial'], $common_args['text2']),
    ),
    'uc_product_registration_rules_condition_regdate_compare' => array(
      'label' => t('Compare the registration date.'),
      'help' => t('TRUE if the registration date meets your criteria.'),
      'module' => $module,
      'arguments' => array($common_args['registered'], $common_args['date2']),
    ),
    'uc_product_registration_rules_condition_purchdate_compare' => array(
      'label' => t('Compare the purchase date.'),
      'help' => t('TRUE if the purchase date meets your criteria.'),
      'module' => $module,
      'arguments' => array($common_args['purchdate'], $common_args['date2']),
    ),
  );
}

/**
 * Compares the registration date to a number
 * @param String $text1
 *    A string to compare $date2 to.
 * @param String $text2
 *    A string to compare $date1 to.
 * @param Array $settings
 *    An array of miscellaneous settings passed to this function.
 * @return bool
 *    Returns TRUE if the registration date meets the comparison criteria.
 */
function uc_product_registration_rules_condition_regdate_compare($date1, $date2, $settings=array()) {
  switch ($settings['operator']) {
    case 'less than':
      return ($date2<$date1);
      break;
    case 'greater than':
      return ($date2>$date1);
      break;
    case 'minimum age':
      return ($date2<=$date1);
      break;
    case 'maximum age':
      return ($date2>=$date1);
      break;
    case 'equal to': // same as default
    default: // direct comparison
      return ($date1==$date2);
  }
}

/**
 * Adds form fields for the comparison of registration dates.
 * @param Array $settings
 * @param Array $form
 */
function uc_product_registration_rules_condition_regdate_compare_form($settings=array(), &$form) {
  $form['settings']['date1']['#title'] = t('Compare this');
  $form['settings']['date2']['#title'] = t('To this');
  $form['settings']['operator'] = array(
    '#type' => 'radios',
    '#title' => t('Date meets one of these criteria'),
    '#default_value' => isset($settings['operator']) ? $settings['operator'] : 0,
    '#options' => array(
      'less than' => t('Less than'),
      'greater than' => t('Greater than'),
      'equal to' => t('The same as'),
      'minimum age' => t('Minimum age'),
      'maximum age' => t('Maximum age'),
    ),
  );
}

/**
 * Compares the purchase date to a number
 * @param String $text1
 *  A string to compare $date2 to.
 * @param String $text2
 *  A string to compare $date1 to.
 * @param Array $settings
 *  An array of miscellaneous settings passed to this function.
 * @return bool
 *  Returns TRUE if the purchase date meets the comparison criteria.
 */
function uc_product_registration_rules_condition_purchdate_compare($date1, $date2, $settings=array()) {
  switch ($settings['operator']) {
    case 'less than':
      return ($date2<$date1);
      break;
    case 'greater than':
      return ($date2>$date1);
      break;
    case 'minimum age':
      return ($date2<=$date1);
      break;
    case 'maximum age':
      return ($date2>=$date1);
      break;
    case 'equal to': // same as default
    default: // direct comparison
      return ($date1==$date2);
  }
}

/**
 * Adds form fields for the comparison of registration dates.
 * @param Array $settings
 * @param Array $form
 */
function uc_product_registration_rules_condition_purchdate_compare_form($settings=array(), &$form) {
  $form['settings']['date1']['#title'] = t('Compare this');
  $form['settings']['date2']['#title'] = t('To this');
  $form['settings']['operator'] = array(
    '#type' => 'radios',
    '#title' => t('Date meets one of these criteria'),
    '#default_value' => isset($settings['operator']) ? $settings['operator'] : 0,
    '#options' => array(
      'less than' => t('Less than'),
      'greater than' => t('Greater than'),
      'equal to' => t('The same as'),
      'minimum age' => t('Minimum age'),
      'maximum age' => t('Maximum age'),
    ),
  );
}

/**
 * Compares input with a serial number.
 * @param String $text1
 *  A string to compare $text2 to.
 * @param String $text2
 *  A string to compare $text1 to.
 * @param Array $settings
 *  An array of miscellaneous settings passed to this function.
 * @return bool
 *  Returns TRUE if the serial number matches
 */
function uc_product_registration_rules_condition_serialnumber_compare($text1, $text2, $settings=array()) {
  return $settings['regex'] ?
    preg_match( sprintf("#%s#ixu", $text1), $text2 )
    : ($text1==$text2);
}

/**
 * Adds form fields for comparison of model numbers.
 * @param Array $settings
 * @param Array $form
 */
function uc_product_registration_rules_condition_serialnumber_compare_form($settings=array(), &$form) {
  $form['settings']['text1']['#title'] = t('Compare this');
  $form['settings']['text2']['#title'] = t('To this');
  $form['settings']['regex'] = array(
    '#type' => 'checkbox',
    '#title' => t('Evaluate the second text as a regular expression'),
    '#default_value' => isset($settings['regex']) ? $settings['regex'] : 0,
  );
}

/**
 * Compares input with a model number.
 * @param String $text1
 *  A string to compare $text2 to.
 * @param String $text2
 *  A string to compare $text1 to.
 * @param Array $settings
 *  An array of miscellaneous settings passed to this function.
 * @return bool
 *  Returns TRUE if the model number matches
 */
function uc_product_registration_rules_condition_modelnumber_compare($text1, $text2, $settings=array()) {
  return $settings['regex'] ? preg_match(sprintf("#%s#ixu", $text1), $text2) : ($text1==$text2);
}

/**
 * Adds form fields for comparison of model numbers.
 * @param Array $settings
 * @param Array $form
 */
function uc_product_registration_rules_condition_modelnumber_compare_form($settings=array(), &$form) {
  $form['settings']['text1']['#title'] = t('Compare this');
  $form['settings']['text2']['#title'] = t('To this');
  $form['settings']['regex'] = array(
    '#type' => 'checkbox',
    '#title' => t('Evaluate the second text as a regular expression'),
    '#default_value' => isset($settings['regex']) ? $settings['regex'] : 0,
  );
}

/**
 * Event Implementation: do something when a product is registered
 * @ingroup rules
 * @param Array $settings
 */
function uc_product_registration_rules_product_registered_event() {
  // @todo I'm not entirely sure what to add here.
}
